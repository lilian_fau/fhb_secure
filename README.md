# FHB Secure

![UCA logo](https://i.ibb.co/tm4tPnc/gdec-gred-limagrain-uca.png)

### Structural and phylogenetic study of effectors secreted by the pathogen *Fusarium graminearum* during wheat fusarium wilt

#

**Author**: [Lilian Faurie](https://www.linkedin.com/in/lilian-faurie-45b998299/) - M2 Bioinformatique ([UCA](https://www.uca.fr/master-bio-informatique))

**Contact**:
- Ludovic Bonhomme (UMR 1095 - [GDEC](https://umr1095.clermont.hub.inrae.fr/organisation/equipes-de-recherche/maladies-des-cereales))
- Christophe Tatout  (UMR 6293 - [iGReD](https://www.igred.fr/equipe/dynamique-de-la-chromatine-et-noyau-3d-coded/))


## Protein-Protein Interactions

This project aims to understand and counteract the infection mechanisms of Fusarium graminearum, the pathogen responsible for Fusarium Head Blight (FHB) in wheat.

**Objectives**: 

- Identify key interactions between fungal effectors and wheat susceptibility proteins.
- Develop new strategies to improve wheat resistance to FHB.

**Methodology**:

- In silico prediction of 3D structures of effectors and their targets using AlphaFold2-based tools ([ColabFold](https://github.com/sokrypton/ColabFold)).
- Phylogenetic analyses to study potential coevolution between effectors and targets.
- Identification of amino acids essential for protein-protein interactions.


This work will guide laboratory experiments to validate protein-protein interactions between effectors and their targets. Ultimately, this research will contribute to the development of wheat varieties more tolerant to Fusarium Head Blight.

**Keywords:** _Fusarium graminearum_, Susceptibility genes, Prediction, Protein structure, PPI, Phylogenetics

## Workflow

This GitLab repository contains all the scripts and Conda environments necessary for data pre-processing, as well as structural and phylogenetic analyses of ColabFold outputs ([Mirdita et al. 2022](https://pubmed.ncbi.nlm.nih.gov/35637307/)). Additionally, a test dataset is provided so you can perform the analyses yourself.


![pipeline](https://i.ibb.co/zbHbywn/Capture-d-cran-2024-06-04-124930.png)

**Figure: _Workflow for analyzing interactions between fungal effectors and wheat target proteins_**

This figure illustrates a comprehensive bioinformatics workflow designed to analyze interactions between fungal effectors and wheat target proteins. The process consists of five main steps:

- **(1) Pre-treatments** involves sequence processing using Bash scripts.
- **(2) Predictions** includes structural predictions using ColabFold batch and DEPICTER2 for disordered regions.
- **(3) Structural analyses** focuses on structural analyses, including the generation of IDR/pLDDT graphs, detection of local protein-protein interaction interfaces, and identification of contact zones.
- **(4) Phylogenetic analyses** is dedicated to phylogenetic analyses, using the MAFFT suite to perform multiple alignments, construct phylogenetic trees, and calculate evolutionary rates, all based on local databases.
- **(5) In silico mutagenesis** consists of in silico mutagenesis aimed at identifying the best effector/target pairs.

The workflow concludes with an in vivo validation step, comparing interacting residues between wild-type and mutant versions of effectors and their targets. This integrated approach, combining structural and evolutionary analyses, aims to thoroughly characterize key interactions between fungal effectors and their wheat targets, paving the way for the development of improved resistance strategies against Fusarium Head Blight.


## Repository

| Folder | Description |
| ------ | ------ |
|conda_envs|Conda environments necessary for the execution of certain scripts|
|dataset_test|Test dataset|
|pre_treatments|Script for pre-processing data upstream of ColabFold structural prediction|
|post_treatments|Scripts for structural and phylogenetic analyzes of ColabFold output data|

## Requirements

List of software dependencies necessary for the execution of all scripts

| Package | Version | Information | 
| -------- | ------- | ----------- | 
|Linux (or WSL2)|24.04|https://www.ubuntu-fr.org/download/|
|Python|3.11.0|https://www.python.org/|
|Conda|24.1.2|https://docs.anaconda.com/free/miniconda/|
|Rstudio|4.4.0|https://posit.co/download/|
|ChimeraX|1.8.0|https://www.cgl.ucsf.edu/chimerax/download.html|
|Colabfold|1.5.5|https://github.com/sokrypton/ColabFold|
|Depicter|2.0.0|http://biomine.cs.vcu.edu/servers/DEPICTER2/|


| Rstudio Package | Version |
| -------- | ------- |
|`ggplot2`|3.4.4|
|`jsonlite`|1.8.8|
|`gridExtra`|2.3|
|`readxl`|1.3.1|
|`ape`|5.8|


## Helps

Wiki : https://gitlab.com/lilian_fau/fhb_secure/-/wikis/home

#

![UCA logo](https://i.ibb.co/gmgwVSR/Logo-text-black.png)



