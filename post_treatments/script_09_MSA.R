# README------------------------------------------------------------------------

# Lilian Faurie - M2 Bioinformatics internship - iGReD - 28/05/24
# This script performs multiple sequence alignment (MSA) on protein sequences from a FASTA file.

# The script performs the following steps:
# 1. Load the necessary libraries.
# 2. Read protein sequences from a FASTA file.
# 3. Perform multiple sequence alignment.
# 4. Print the alignment results.
# 5. Generate a formatted PDF of the alignment.
# 6. Provide guidance for handling wide alignments in LaTeX.

# LIBRARIES---------------------------------------------------------------------

# Developed under R - 4.4.0
# Required packages: BiocManager - 1.30.23, msa, tinytex
# Load the necessary packages
# install.packages(c("BiocManager","tinytex"))

if (!requireNamespace("BiocManager", quietly=TRUE))
  install.packages("BiocManager")
BiocManager::install("msa")

library(msa)
library(tinytex)

# CODE-------------------------------------------------------------------------

# Read protein sequences from a FASTA file
HMA <- readAAStringSet("HMA.fa")
HMA
# Perform multiple sequence alignment
HMA_aln <- msa(HMA)
HMA_aln
# Print the alignment results
print(HMA_aln, show="complete")

# For a specific region: y = c(200, 350)
msaPrettyPrint(HMA_aln, output="pdf", showNames="left", consensusColors="Gray",
               logoColors="rasmol", shadingMode="identical",
               showConsensus="bottom", shadingColors="blues", shadingModeArg=80,
               showLegend=TRUE, askForOverwrite=FALSE)

# If the alignment to be printed with msaPrettyPrint() is wide (thousands of columns or wider),
# LaTeX may terminate prematurely due to exceeded capacity. We recommend the following steps if problems arise:

# 1. Run pdflatex on the generated .tex file to determine if it is a TEX capacity issue.
# 2. If so, split the alignment into multiple chunks and run msaPrettyPrint() on each chunk separately.

# Example for handling wide alignments with multiple chunks:
chunkSize <- 300 
for (start in seq(1, ncol(HMA_aln), by = chunkSize)) { 
  end <- min(start + chunkSize - 1, ncol(HMA_aln))
  alnPart <- AAMultipleAlignment(subseq(unmasked(HMA_aln), start, end))
  msaPrettyPrint(x = alnPart, output = "pdf", subset = NULL, file = paste0("HMA_aln_", start, "-", end, ".pdf"))
}