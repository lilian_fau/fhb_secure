# README------------------------------------------------------------------------------------------

# Lilian Faurie - M2 Bionformatics internship - iGReD - 10/07/24 
# ChimeraX - version 1.8 (2024-06-10)

# This script processes PDB files to detect and analyze contact zones between effector and target proteins.
# It performs the following steps:
# 1. Opens each PDB file and identifies the effector and target proteins.
# 2. Changes chain IDs and visualizes the structures in ChimeraX.
# 3. Detects contact zones at specified distances (3Å, 5Å, 7Å) and writes this information to output files.
# 4. Combines the contact zone data to generate interaction tables for each effector-target pair.
# 5. Saves modified models and generates detailed interaction tables in TSV format.

# DEPENDENCIES------------------------------------------------------------------------------------

# This script requires the following Python packages and ChimeraX tool:
# - os
# - chimerax.core.commands

# To install ChimeraX, follow the instructions on the official ChimeraX website:
# https://www.cgl.ucsf.edu/chimerax/download.html

# IMPORTS----------------------------------------------------------------------------------------

# REQUIREMENT => Launch ChimeraX (1.8) > Tools > General > Shell > pip install pandas

import os
from chimerax.core.session import Session
from chimerax.core.commands import run
import pandas as pd
import re
import csv
from collections import defaultdict

# CONFIG------------------------------------------------------------------------------------------

# Directories setup
structures_dir = r"C:\Users\lilia\Desktop\Stage\DATA\EFF03\structures_of_int"
fasta_dir = r"C:\Users\lilia\Desktop\Stage\DATA\EFF03\structures_of_int"
results_dir = r"C:\Users\lilia\Desktop\Stage\DATA\EFF03\contact_zones"

# Ensure the directories exist
os.makedirs(structures_dir, exist_ok=True)
os.makedirs(results_dir, exist_ok=True)

# List of PDB files
structure_files = [f for f in os.listdir(structures_dir) if f.endswith('.pdb')]

# FUNCTIONS---------------------------------------------------------------------------------------

def detect_and_write_contact_zones(session, ID_effector, ID_target, output_dir, distances=[3, 5, 7]):
    # Detects contact zones between effector and target at specified distances and writes to output files
    for distance in distances:
        run(session, f"select ((/{ID_effector} & ::polymer_type>0 ) & ((/{ID_target} & ::polymer_type>0 ) :<{distance})) | ((/{ID_target} & ::polymer_type>0 ) & ((/{ID_effector} & ::polymer_type>0 ) :<{distance}))")
        output_file = os.path.join(output_dir, f'contact_zones_{distance}Å_{ID_target}_x_{ID_effector}.txt')
        run(session, f"info residues sel saveFile {output_file}")
        print(f"Contact zones for {distance}Å written to {output_file}")

def process_pdb_file(session, pdb_file):
    # Open the PDB file and get the model
    full_path = os.path.join(structures_dir, pdb_file)
    run(session, f"open {full_path}")
    model = session.models[-1]
    
    # Extract IDs from the filename
    ID1, ID2 = os.path.basename(pdb_file).split('_x_')[0], os.path.basename(pdb_file).split('_x_')[1].split('_')[0]
    
    # Read the corresponding FASTA file
    fasta_filename = f"{ID1}_x_{ID2}.fasta"
    fasta_path = os.path.join(fasta_dir, fasta_filename)
    
    with open(fasta_path, "r") as fasta_file:
        seq1, seq2 = fasta_file.read().split('\n', 1)[1].split(':')
    
    # Get lengths from ChimeraX
    chain_A_length = len(run(session, "select /A").residues)
    chain_B_length = len(run(session, "select /B").residues)
    
    # Compare lengths and assign IDs with tolerance of +/- 1 AA
    if abs(len(seq1) - chain_A_length) <= 1 and abs(len(seq2) - chain_B_length) <= 1:
        chain_A_id, chain_B_id = ID1, ID2
        chain_A_length, chain_B_length = len(seq1), len(seq2)
    elif abs(len(seq2) - chain_A_length) <= 1 and abs(len(seq1) - chain_B_length) <= 1:
        chain_A_id, chain_B_id = ID2, ID1
        chain_A_length, chain_B_length = len(seq2), len(seq1)
    else:
        raise ValueError(f"Mismatch in sequence lengths: FASTA ({len(seq1)}, {len(seq2)}) vs PDB ({chain_A_length}, {chain_B_length})")

    # Change chain IDs in ChimeraX
    run(session, f"changechains /A {chain_A_id}")
    run(session, f"changechains /B {chain_B_id}")
    
    # Create output directory for contact zones
    output_dir = os.path.join(results_dir, f"{chain_A_id}_x_{chain_B_id}")
    os.makedirs(output_dir, exist_ok=True)
       
    # Color chains for visualization and adjust visualization settings
    colors = ['red', 'skyblue']
    for i, chain_id in enumerate(model.chains):
        run(session, f"color /{chain_id.chain_id} {colors[i % len(colors)]}")
    
    # Adjust visualization settings
    run(session, "set bgColor white; graphics silhouettes true; lighting shadows true intensity 0.5; lighting simple")
    run(session, f"select /{chain_B_id}:1 /{chain_A_id}:1; label sel text {{0.chain_id}}; label height 2")
    run(session, f"select ((/{chain_B_id} & ::polymer_type>0 ) & ((/{chain_A_id} & ::polymer_type>0 ) :<7)); color sel yellow")
    run(session, f"select ((/{chain_A_id} & ::polymer_type>0 ) & ((/{chain_B_id} & ::polymer_type>0 ) :<7)); color sel green")
    
     # Detect and write contact zones, save modified model, and close the model in ChimeraX
    detect_and_write_contact_zones(session, chain_B_id, chain_A_id, output_dir)
    run(session, f"save {os.path.join(output_dir, f'modified_{chain_A_id}_x_{chain_B_id}.cxs')}")
    session.models.close([model])
    
    return chain_B_id, chain_A_id, chain_B_length, chain_A_length

def read_data(filename, ID_effector, ID_target):
    # Reads contact zone data from a file and organizes it into dictionaries for effector and target
    data_effector = defaultdict(lambda: {'3': False, '5': False, '7': False, 'aa_type': ''})
    data_target = defaultdict(lambda: {'3': False, '5': False, '7': False, 'aa_type': ''})
    
    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split()
            if len(parts) >= 5:
                distance = parts[0]
                residue_id = parts[3]
                position = int(residue_id.split(':')[1])
                aa_type = parts[5]
                if ID_effector in residue_id:
                    data_effector[position][distance] = True
                    data_effector[position]['aa_type'] = aa_type
                elif ID_target in residue_id:
                    data_target[position][distance] = True
                    data_target[position]['aa_type'] = aa_type
    
    return data_effector, data_target

def prepare_data(data, total_length):
    # Prepares data for output to a table
    prepared_data = []
    for position in range(1, total_length + 1):
        row = [
            'X' if data[position]['3'] else '',
            'X' if data[position]['5'] else '',
            'X' if data[position]['7'] else '',
            position,
            data[position]['aa_type']
        ]
        prepared_data.append(row)
    return prepared_data

def process_contact_zones(results_dir, lengths_dict):
    # Processes all contact zones to generate interaction tables
    for subdir in os.listdir(results_dir):
        subdir_path = os.path.join(results_dir, subdir)
        if os.path.isdir(subdir_path):
            effector_length, target_length = lengths_dict.get(subdir, (None, None))
            if effector_length is not None and target_length is not None:
                ID_target, ID_effector = subdir.split('_x_')

                # Combine data from multiple files
                data_frames = []
                for file_name in os.listdir(subdir_path):
                    if file_name.endswith(".txt"):
                        file_path = os.path.join(subdir_path, file_name)
                        angstrom_distance = int(re.search(r'(\d+)Å', file_name).group(1))
                        df = pd.read_csv(file_path, delimiter="\t", header=None)
                        df.insert(0, 'angstrom_distance', angstrom_distance)
                        data_frames.append(df)

                # Write combined data to a temporary file
                if data_frames:
                    combined_df = pd.concat(data_frames, ignore_index=True)
                    combined_data_path = os.path.join(subdir_path, "tmp_data.txt")
                    combined_df.to_csv(combined_data_path, sep="\t", index=False, header=False)

                    # Read and prepare data for effector and target
                    data_effector, data_target = read_data(combined_data_path, ID_effector, ID_target)
                    prepared_effector = prepare_data(data_effector, effector_length)
                    prepared_target = prepare_data(data_target, target_length)

                    # Determine the maximum number of rows needed
                    max_rows = max(len(prepared_effector), len(prepared_target))

                    # Write the interaction table to a TSV file
                    output_filename = os.path.join(subdir_path, f"interaction_table_{ID_target}_x_{ID_effector}.tsv")
                    with open(output_filename, 'w', newline='') as tsv_file:
                        writer = csv.writer(tsv_file, delimiter='\t')
                        writer.writerow([f'{ID_effector} ({effector_length}AA)', '', '', '', '','', f'{ID_target} ({target_length}AA)'])
                        writer.writerow(['<3Å', '<5Å', '<7Å', 'Position', 'AA_type', '', '<3Å', '<5Å', '<7Å', 'Position', 'AA_type'])
                        for i in range(max_rows):
                            eff_row = prepared_effector[i] if i < len(prepared_effector) else ['', '', '', '', '']
                            target_row = prepared_target[i] if i < len(prepared_target) else ['', '', '', '', '']
                            writer.writerow(eff_row + [''] + target_row)

                    print(f"TSV table generated in {output_filename}")

                    # Remove the temporary file
                    os.remove(combined_data_path)
            else:
                print(f"Warning: Lengths not found for {subdir}")

# EXECUTION---------------------------------------------------------------------------------------
# Main execution block
lengths_dict = {}
for pdb_file in structure_files:
    chain_B_id, chain_A_id, chain_B_length, chain_A_length = process_pdb_file(session, pdb_file)
    lengths_dict[f"{chain_A_id}_x_{chain_B_id}"] = (chain_B_length, chain_A_length)

# Process contact zones after all PDB files have been processed
process_contact_zones(results_dir, lengths_dict)
