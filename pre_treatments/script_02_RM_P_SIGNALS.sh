#!/bin/bash

# README------------------------------------------------------------------------------------------

# Lilian Faurie - M2 Bionformatics internship - iGReD - 02/05/24

# This script removes the signal peptide sequence from the input files and generates output
# files in FASTA format. The script takes one or more input files in TXT or TSV format, checks
# their readability and extension, converts them to Unix format using dos2unix, and processes
# them to remove the signal peptide sequence. The output files have the same name as the input
# files, but with the extension "_no_p_signal.fasta".

#  1. Check if input files are provided and valid (readable and with correct extension)
#  2. Convert input files to Unix format using `dos2unix`
#  3. Define a function to process each input file:
#      a. Remove the signal peptide sequence from each protein sequence
#      b. Write the processed sequences to an output file in FASTA format
#  4. Call the processing function for each input file

# DEPENDENCIES------------------------------------------------------------------------------------

# This script requires the following tools:
# - dos2unix

# To install this tool, use the following command (Ubuntu/Debian):
# sudo apt-get update
# sudo apt-get install dos2unix

# CONFIG------------------------------------------------------------------------------------------

# Check that files are provided as arguments
if [ $# -eq 0 ]; then
    echo "No files provided as arguments."
    echo "Usage: $0 --file <file1> [<file2> ...]"
    exit 1
fi

# Parse the arguments
FILES=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        --file)
        shift # past argument
        while [[ $# -gt 0 ]]; do
            FILES+=("$1")
            shift # past value
        done
        break
        ;;
    esac
done

# Check that the files exist and are readable
for FILE in "${FILES[@]}"; do
    if [ ! -r "${FILE}" ]; then
        echo "The file '${FILE}' does not exist or is not readable."
        exit 1
    fi

    # Check the file extension
    file_ext="${FILE##*.}"
    if [[ "${file_ext}" != "txt" && "${file_ext}" != "tsv" ]]; then
        echo "The file '${FILE}' must be in TXT or TSV format."
        exit 1
    fi
done

# Convert input files to Unix format using dos2unix
for FILE in "${FILES[@]}"; do
    dos2unix --quiet "${FILE}"
done

# Define the function to process an input file
function process_file {
    FILE="$1"
    # Create an output file with the same name as the input file, but with the extension .fasta
    output_file="${FILE%.*}_no_p_signal.fasta"

    echo -ne "Processing ${FILE}\t>>>\t"

    # Process all lines in the file, including the header
    while IFS=$'\t' read -r name signal_length seq; do
        # Remove the signal peptide sequence from the sequence
        new_seq="${seq:$signal_length}"
        # Write the FASTA identifier and the processed sequence to the output file
        echo -e ">${name}_no_p_signal\n$new_seq" >> "$output_file"
    done < "${FILE}"

    # Display a message to indicate that the file has been processed
    echo "OK"
}

echo -e "\n#### P-SIGNAL REMOVER ###############################################"
echo -e "\n---- INFORMATION ----------------------------------------------------\n"

# Display the detected input files
echo -n "INPUT : "
for FILE in "${FILES[@]}"; do
    echo -ne "${FILE}\t"
done
echo ""

# Display the output files
echo -n "OUTPUT : "
for FILE in "${FILES[@]}"; do
    echo -ne "${FILE%.*}_no_p_signal.fasta\t"
done
echo ""

echo -e "\n---- TREATMENT ------------------------------------------------------\n"

# Call the function to process each input file
for FILE in "${FILES[@]}"; do
    process_file "${FILE}"
done

echo -e "\n---- DONE -----------------------------------------------------------"
