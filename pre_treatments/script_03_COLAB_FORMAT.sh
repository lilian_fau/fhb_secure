#!/bin/bash

# README------------------------------------------------------------------------------------------

# Lilian Faurie - M2 Bionformatics internship - iGReD - 02/05/24

# This script is designed to process FASTA files containing effector and target sequences.
# It first checks that the input files are present, then creates output directories.
# Then, it generates files for monomers (effectors and targets separately) and multimers (combining effectors and targets).

# 1. Argument and input file checking: The script checks that the necessary arguments are provided and that the input files exist.
# 2. Output directory creation: The script creates the directories where the results will be stored.
# 3. Monomer file generation: The script processes the effector and target files separately to generate files for each sequence.
# 4. Multimer file generation: The script combines each effector sequence with each target sequence to generate multimer files.

# DEPENDENCIES------------------------------------------------------------------------------------

# This script requires the following tools:
# - dos2unix

# To install this tool, use the following command (Ubuntu/Debian):
# sudo apt-get update
# sudo apt-get install dos2unix

# CODE--------------------------------------------------------------------------------------------

# Argument parsing
if [ "$#" -lt 4 ]; then
    echo "Usage: $0 --eff <effectors_fasta_file> --target <targets_fasta_file>"
    exit 1
fi

while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        --eff)
        effectors="$2"
        shift # past argument
        shift # past value
        ;;
        --target)
        targets="$2"
        shift # past argument
        shift # past value
        ;;
        *)
        echo "Invalid option: $1"
        exit 1
        ;;
    esac
done

# Input file checking
if [ ! -f "${effectors}" ] || [ ! -f "${targets}" ]; then
    echo "Error: One or more input files are missing."
    exit 1
fi

# Convert input files to Unix format using dos2unix
dos2unix --quiet "${effectors}" "${targets}"

MONOMER_EFF_INPUT_FASTA_DIR="Monomers/EFF/01_input_fasta"
MONOMER_EFF_STRUCTURE_RESULTS="Monomers/EFF/02_structure_results"

MONOMER_LOC_INPUT_FASTA_DIR="Monomers/LOC/01_input_fasta"
MONOMER_LOC_STRUCTURE_RESULTS="Monomers/LOC/02_structure_results"

MULTIMER_EFF_x_LOC_INPUT_FASTA_DIR="Multimers/EFF_x_LOC/01_input_fasta"
MULTIMER_EFF_x_LOC_STRUCTURE_RESULTS="Multimers/EFF_x_LOC/02_structure_results"

# Output directory creation
mkdir -p "${MONOMER_EFF_INPUT_FASTA_DIR}" "${MONOMER_EFF_STRUCTURE_RESULTS}" "${MONOMER_LOC_INPUT_FASTA_DIR}" "${MONOMER_LOC_STRUCTURE_RESULTS}" "${MULTIMER_EFF_x_LOC_INPUT_FASTA_DIR}" "${MULTIMER_EFF_x_LOC_STRUCTURE_RESULTS}"

echo -e "\n#### MONO/MULTIMERS FORMAT ##########################################"

# Function to generate monomer files
generate_monomer_files() {
    input_file="$1"
    output_dir="$2"
    count=1
    total=$(grep -c '^>' "$input_file")
    awk -v count="$count" -v total="$total" -v output_dir="$output_dir" '/^>/{if(f) close(f); f=output_dir"/"substr($0, 2)".fasta"; name=substr($0, 2); print "[" count++ "/" total "] " name > "/dev/stderr"; print $0 > f; next} {print >> f}' "$input_file"
}

# Function to generate multimer files
generate_multimer_files() {
    eff_dir="$1"
    cib_dir="$2"
    output_dir="$3"
    count=1
    num_effs=$(ls -1 "${eff_dir}"/*.fasta 2>/dev/null | wc -l)
    num_cibs=$(ls -1 "${cib_dir}"/*.fasta 2>/dev/null | wc -l)
    total=$((num_effs*num_cibs))

    for eff in "${eff_dir}"/*.fasta; do
        for cib in "${cib_dir}"/*.fasta; do
            eff_name=$(basename "$eff" .fasta)
            cib_name=$(basename "$cib" .fasta)
            multimer_name="${eff_name}_x_${cib_name}"
            echo -ne "[${count}/${total}]\t${multimer_name}"
            echo -e ">${multimer_name}\n$(cat "$eff" | sed '1d'):$(cat "$cib" | sed '1d')" > "${output_dir}/${multimer_name}.fasta"
            echo -e "\t>>>\tOK"
            count=$((count+1))
        done
    done
}

echo -e "\n---- FASTA MONOMERS -------------------------------------------------"

# Generate monomer files for effectors
echo -e "\nProcessing effectors file: $effectors\n"
generate_monomer_files "$effectors" "${MONOMER_EFF_INPUT_FASTA_DIR}"

# Generate monomer files for targets
echo -e "\nProcessing targets file: $targets\n"
generate_monomer_files "$targets" "${MONOMER_LOC_INPUT_FASTA_DIR}"

echo -e "\n---- FASTA MULTIMERS ------------------------------------------------"

# Generate multimer files for effectors and targets
echo -e "\nProcessing files: ${effectors}_x_${targets}\n"
generate_multimer_files "${MONOMER_EFF_INPUT_FASTA_DIR}" "${MONOMER_LOC_INPUT_FASTA_DIR}" "${MULTIMER_EFF_x_LOC_INPUT_FASTA_DIR}"

echo -e "\n---- DONE -----------------------------------------------------------"