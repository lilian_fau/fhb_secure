#!/bin/bash

# README------------------------------------------------------------------------------------------

# Lilian Faurie - M2 Bionformatics internship - iGReD - 02/05/24

# This script downloads and processes monomer sequences in .fasta format corresponding to the provided accession numbers from a supplementary files.

# This script follows these steps:
#   1. Verification and processing of command line arguments
#   2. Validation of the specified database
#   3. Downloading and processing of monomer sequences
#   4. Error and information logging

# DEPENDENCIES------------------------------------------------------------------------------------

# This script requires the following tools:
# - wget
# - dos2unix
# - uuidgen

# To install these tools, use the following commands (Ubuntu/Debian):
# sudo apt-get update
# sudo apt-get install wget dos2unix uuid-runtime

# CONFIG------------------------------------------------------------------------------------------

# Parse command line arguments
input_files=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        --file)
        shift # past argument
        while [[ $# -gt 0 ]] && [[ ! $1 =~ ^- ]]; do
            input_files+=("$1")
            shift
        done
        ;;
        *)
        echo "Unknown option: $1"
        exit 1
        ;;
    esac
done

# Check if the input files are specified
if [ ${#input_files[@]} -eq 0 ]; then
    echo "Usage: $0 --file <accession_list_file1> <accession_list_file2> ..."
    exit 1
fi

# Array of valid databases
valid_databases=(
    "afdb"
    "cdp"
    "chembl"
    "edam"
    "emdb"
    "ena_coding"
    "ena_geospatial"
    "ena_noncoding"
    "ena_rrna"
    "ena_sequence"
    "ena_sequence_con"
    "ena_sequence_conexp"
    "ena_sva"
    "ensemblgene"
    "ensemblgenomesgene"
    "ensemblgenomestranscript"
    "ensembltranscript"
    "epo_prt"
    "hgnc"
    "imgthlacds"
    "imgthlagen"
    "imgthlapro"
    "imgtligm"
    "interpro"
    "ipdkircds"
    "ipdkirgen"
    "ipdkirpro"
    "ipdmhccds"
    "ipdmhcgen"
    "ipdmhcpro"
    "ipdnhkircds"
    "ipdnhkirgen"
    "ipdnhkirpro"
    "iprmc"
    "iprmcuniparc"
    "jpo_prt"
    "kipo_prt"
    "medline"
    "mp"
    "mpep"
    "mpro"
    "nrnl1"
    "nrnl2"
    "nrpl1"
    "nrpl2"
    "patent_equivalents"
    "pdb"
    "pdbekb"
    "refseqn"
    "refseqp"
    "taxonomy"
    "uniparc"
    "uniprotkb"
    "uniref100"
    "uniref50"
    "uniref90"
    "unisave"
    "uspto_prt"
)



# Initialize the file counter before the loop
input_file_counter=1

# Process each input file
for input_file in "${input_files[@]}"; do
    # Check if the input file exists
    if [ ! -f "${input_file}" ]; then
        echo "Error: The input file ${input_file} does not exist."
        continue
    fi
    
    # Convert input files to Unix format using dos2unix
    dos2unix --quiet "${input_file}"
    
    # Generate a unique random identifier
    unique_id=$(uuidgen | tr -d '-' | head -c 5)

    # Destination folders for downloaded files
    destination_folder="monomer_${unique_id}"
    fasta_output_folder="${destination_folder}/input_fasta"
    structures_folder="${destination_folder}/structures_results"

    
    # Create the folders if they do not exist
    mkdir -p "${destination_folder}" "${fasta_output_folder}" "${structures_folder}"

    # Database configuration
    specified_database=$(head -n1 "${input_file}")

    # Check if the database is valid
    if [[ ! " ${valid_databases[@]} " =~ " ${specified_database} " ]]; then
        echo -e "\n---- INFORMATION ----------------------------------------------------\n"
        echo -e "INPUT: ${input_file} [${input_file_counter}/${#input_files[@]}]\n"
        echo "Error: Invalid database '${specified_database}'. Supported databases are: ${valid_databases[*]}"
        # Increment the file counter after processing each file
        input_file_counter=$((input_file_counter + 1))
        continue
    fi

    echo -e "\n---- INFORMATION ----------------------------------------------------"

    # LOGS--------------------------------------------------------------------------------------------

    # Error logging
    log_file="${destination_folder}/log.txt"
    touch "${log_file}"
    exec > >(tee -a "${log_file}") 2>&1

    # DOWNLOAD & TREATMENTS---------------------------------------------------------------------------

    # Function to retrieve protein sequences of genes
    get_sequence() {
        local accession="$1"
        local url="https://www.ebi.ac.uk/Tools/dbfetch/dbfetch?db=${specified_database}&id=${accession}&format=fasta&style=raw&Retrieve=Retrieve"
        local filename="${fasta_output_folder}/${accession}.fasta"

        # Download the sequence
        wget -q -O "${filename}" "${url}"

        # Check if the first line contains ">ID" using regex
        if grep -qE "^>[[:alnum:]]+" "${filename}"; then

            # Remove "status=active" in all .fasta files except the current file + Add the accession number
            sed -i -E 's/status=.*//g; s/^>[[:alnum:]]+/& [gene='"${accession}"']/;' "${filename}"

            # Use AWK to keep the longest sequence and the ID separately and store it in variables
            fasta=$(awk '/^>/ { if (length(seq) > length(longest_seq)) { longest_seq=seq; ID=id; } seq=""; id=$0; next; } { seq = seq $0; } END { if (length(seq) > length(longest_seq)) { longest_seq = seq; ID = id; } print longest_seq, ID }' "${filename}")

            ID=$(echo "${fasta}" | awk '{for (i=2; i<=NF; i++) printf "%s ", $i; print ""}')
            sequence=$(echo "${fasta}" | awk '{print $1}')

            # Create the file with the longest sequence
            echo -e "${ID}\n${sequence}\n" > "${fasta_output_folder}/${accession}.fasta"

            # Concatenate all files into one
            echo -e "${ID}\n${sequence}" >> "${fasta_output_folder}/summary_sequences.fasta"
        fi
    }

    # FILES & VERIFICATIONS---------------------------------------------------------------------------

    # List to store the names of successfully downloaded files
    downloaded_files=()

    # Use sort -u to extract unique accession numbers and avoid redundancy in database queries
    unique_accessions=$(tail -n +2 "${input_file}" | sort -u)

    # Display a message

    echo -e "\nINPUT: ${input_file} [${input_file_counter}/${#input_files[@]}]"
    echo -e "OUTPUT: ${fasta_output_folder}"
    echo -e "DATABASE: ${specified_database}"
    echo -e "ACCESSIONS: $(wc -l <<< "${unique_accessions}") "
    echo -e "\n---- DOWNLOAD -------------------------------------------------------\n"

    # Initialize the counter
    accession_counter=1

    # Read each line of the file containing unique accession numbers and invoke get_sequence
    while read -r accession; do
        # Display the counter and the filename
        echo -ne "[${accession_counter}/$(wc -l <<< "${unique_accessions}")]\t\t${accession}.fasta"

        get_sequence "${accession}"

        file="${fasta_output_folder}/${accession}.fasta"

        if [ -e "${file}" ] && grep -q "No entries found" "${file}" || grep -q "ERROR" "${file}" ; then
            echo -e "\t >>> \t ERROR  [No entries found]"
        else
            if [ -e "${file}" ]; then
                echo -e "\t >>> \t OK"
                downloaded_files+=("${file}")
            fi
        fi

        # Increment the counter
        accession_counter=$((accession_counter + 1))
    done <<< "${unique_accessions}"

    file_count=$(find "${fasta_output_folder}" -type f | wc -l)
    file_count=$((file_count - 1))

    # Increment the file counter after processing each file
    input_file_counter=$((input_file_counter + 1))
done

echo -e "\n---- DONE -----------------------------------------------------------"
